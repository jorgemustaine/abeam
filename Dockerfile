FROM ubuntu:latest
MAINTAINER "jorgescalona@riseup.net"

ENV DEBIAN_FRONTEND=noninteractive
USER  root
WORKDIR /root

SHELL ["/bin/bash", "-c"]


RUN apt-get update \
  && apt-get install -y python3-pip python3-dev \
  && cd /usr/local/bin \
  && ln -s /usr/bin/python3 python \
  && pip3 --no-cache-dir install --upgrade pip \
  && rm -rf /var/lib/apt/lists/*

RUN apt-get -qq -y update && \
    DEBIAN_FRONTEND=noninteractive apt-get -qq -y install \
        gcc \
        g++ \
        zlibc \
        zlib1g-dev \
        libssl-dev \
        libbz2-dev \
        libsqlite3-dev \
        libncurses5-dev \
        libgdbm-dev \
        libgdbm-compat-dev \
        liblzma-dev \
        libreadline-dev \
        uuid-dev \
        libffi-dev \
        tk-dev \
        wget \
        curl \
        git \
        make \
        sudo \
        bash-completion \
        tree \
        vim \
        software-properties-common && \
        apt-get -y install build-essential wget curl vim git aptitude && \
        curl https://j.mp/spf13-vim3 -L > spf13-vim.sh && sh spf13-vim.sh && \
        mv /usr/bin/lsb_release /usr/bin/lsb_release.bak && \
        apt-get -y autoclean && apt-get -y autoremove && \
        rm -rf /var/lib/apt/lists/*

RUN aptitude update && aptitude -y upgrade && \
    aptitude -y install iputils-ping && \
    curl https://packages.cloud.google.com/apt/doc/apt-key.gpg | apt-key --keyring /usr/share/keyrings/cloud.google.gpg add -

#  RUN aptitude update && aptitude -y install google-cloud-sdk

EXPOSE 80
EXPOSE 22
ENTRYPOINT ["/bin/bash"]
