import argparse
import apache_beam as beam
from apache_beam.options.pipeline_options import PipelineOptions
from unidecode import unidecode


def normalize_words(word):
    """This method verified if the words are repeat with diff capitalize or
    contain different signs """
    signs = [',', '.', '-', ';', ':', ' ', '\'', '"']
    for sign in signs:
        word = word.replace(sign, '')

    word = unidecode(word.lower())
    return word


def main():
    """Define args and another global keys"""

    parser = argparse.ArgumentParser(
        usage=("python pipeline_ex.py -i file_path [-o output_file] [-n \
number_of_words] [--runner abeam_runner]"),
        description='''Parser your Apache beam to GCP platform Dataflow
        service''',
        epilog="A script by infrastructure to everybody else!")

    parser.add_argument(
        "-i", "--input_file", required=True,
        help="datasource path, that can be a bucket", metavar="<source_path>"
    )
    parser.add_argument(
        "-o", "--output_file", default="std_out",
        help="data destination path, that can be a bucket",
        metavar="<dest_path>"
    )
    parser.add_argument(
        "-n", "--n_words", type=int, metavar="N", default=5,
        help="Integer Number of word that you want to count. \"5\" For the\
        firsts five for example"
    )
    # the line above return all args

    custom_args, beam_args = parser.parse_known_args()
    run_pipeline(custom_args, beam_args)


def run_pipeline(custom_args, beam_args):
    """executed the pipeline for data ingest and transform it"""
    # my defined arguments

    input_file = custom_args.input_file
    output_file = custom_args.output_file
    n_words = custom_args.n_words
    #  abeam args
    opts = PipelineOptions(beam_args, save_main_session=True)

    # This pipeline count the number of words in Don Quijote text
    with beam.Pipeline(options=opts) as pipeline:
        #  We obtain our first PCollection from file sample by lines
        #  "En un lugar de la mancha ...."
        lines = pipeline | beam.io.ReadFromText(input_file)
        # extract words and the FlatMap make a squash of lists or collections
        words = lines | beam.FlatMap(lambda l: l.split())
        norm_words = words | beam.Map(normalize_words)
        # return a PCollection -----> (str, int) -----> tuple
        word_count = norm_words | beam.combiners.Count.PerElement()
        #  the key arg say to combiner what is the element to count
        top_words_list = word_count | beam.combiners.Top.Of(
            n_words, key=lambda kv: kv[1])
        top_words = top_words_list | beam.FlatMap(lambda x: x)
        words_with_format = top_words | beam.Map(lambda kv: "%s,%d" % (
            kv[0], kv[1]
        ))
        words_with_format | beam.Map(print)
        words_with_format | beam.io.WriteToText(output_file)


if __name__ == '__main__':
    main()
