[![License: CC BY 4.0](https://img.shields.io/badge/License-CC%20BY%204.0-lightgrey.svg)](https://creativecommons.org/licenses/by/4.0/)

# Apache Beam
![abeam](static/images/abeam_ico.png)

This is a sample to display a presentation with [Markdown slideshare](https://mark.show/).

This sample it's for explain how you can deploy a pipeline in dataflow on GCP
with python, In this time I seek the talk from [Sotware Guru](https://www.youtube.com/channel/UCGZsBwp-Azah4Wdp9x6EzFQ) the same It's
an [CC by license](https://creativecommons.org/2011/04/15/plaintext-versions-of-creative-commons-licenses-and-cc0/)

**Apache Beam:** It's an opensource framework of data for defining both batch and
streaming data parallel processing pipelines. With abeam SDK, you can build a
program that then is executed by one of Beam's supported distributed backends.

### How deploy your presentation:

~~~
curl -F file=@filename.md https://mark.show
# or
cat filename.md | curl -F 'data=<-' https://mark.show
# or
echo "# Slide md text" | curl -F 'data=<-' https://mark.show
~~~

### some key commands

**for create bucket with gsutil:**
`gsutil mb -p <project_name> -l us-east1 -c STANDARD -b on gs://bucket_name`

**asign labes to bucket:**
`gsutil label ch -l key-foo:value-bar gs://example-bucket`


### interesting links:

* [markdown publish](https://mark.show/5eeb54a4381f5#/)
* [Advance Slide presentation](https://mark.show/https://mark.show/demo.md#)
* [tuto apache beam + python](https://www.youtube.com/watch?v=xSgTsKWhU0Y)
* [gcloud auth with json credentials](https://cloud.google.com/vision/automl/docs/quickstart)
* [argparse documentation lib](https://docs.python.org/3/library/argparse.html#adding-arguments)
* [abeam runners](https://beam.apache.org/documentation/runners)
* [Python pipeline dependencies](https://beam.apache.org/documentation/sdks/python-pipeline-dependencies/)

`gcloud auth activate-service-account serviceaccountm --key-file=$GOOGLE_APPLICATION_CREDENTIALS`


### **example for run the script:**
**on local:**
`python pipeline_ex.py --input_file data/muestra.txt --output_file demo.txt --runner DirectRunner`

**on gcp:**
`python pipeline_ex.py --input_file data/muestra.txt --output_file demo.txt --runner DirectRunner`

**note:** It's very important define python dependencies with abeam parameter 
`--requirements_file requirements.txt` for install it. For GCP execution We
should mute `apache-beam[gcp]` in that file.

**about me:**

* jorgeescalona.globant@latam.com
* @jorgemustaine

2021
