#  Apache Beam

by [@jorgemustaine](https://twitter.com/jorgemustaine) 2021

---
#  Apache Beam

In 2015 google release a standard for ingest data with fast transfer rates for
big-data process and Machine learning. This standard is [dataflow model](https://research.google.com/pubs/archive/43864.pdf)

The Apache Beam SDKs It's a set of agnostic tools for create data processing
pipelines and give to ingest of data with a few runners like:

* google [dataflow](https://cloud.google.com/dataflow/docs) service.
* apache [Flink](https://flink.apache.org/) Data Streams.
* apache [Spark](https://spark.apache.org/) light analytics engine.
* apache [hadoop](https://hadoop.apache.org/) framework.

---

# Apache Beam SDK's

You can write your apache beam pipelines in any language for exmaple:

1. [java](https://beam.apache.org/get-started/quickstart-java/)
1. [*python**](https://beam.apache.org/get-started/quickstart-py/)
1. [go](https://beam.apache.org/documentation/sdks/go/)
1. Spotify scio [scala](https://github.com/spotify/scio)

---

# The third SlideShow

